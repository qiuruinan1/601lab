# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Menu(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    big_type = models.CharField(max_length=32, blank=True, null=True)
    type = models.CharField(max_length=32, blank=True, null=True)
    name = models.CharField(max_length=32, blank=True, null=True)
    img = models.CharField(max_length=255, blank=True, null=True)
    peiliao = models.CharField(max_length=255, blank=True, null=True)
    zz = models.CharField(max_length=25, blank=True, null=True)
    imgze = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)
    sc = models.CharField(max_length=36, blank=True, null=True)
    cat = models.CharField(max_length=32, blank=True, null=True)
    num = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        # managed = False # 这个设置为False表示不用django的migrations机制来管理数据表
        db_table = 'menu'



class User(models.Model):
    id = models.CharField(primary_key=True, max_length=31)
    name = models.CharField(max_length=32, blank=True, null=True)
    password = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'user'

# 实践域相关表
class Company(models.Model):
    name = models.CharField(max_length=100)
    number_of_employees = models.IntegerField()
    industry_type = models.CharField(max_length=100)
    grade = models.CharField(max_length=50)

    def __str__(self):
        return self.name
    

class Evaluation(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    grade = models.CharField(max_length=50)
    form = models.CharField(max_length=50)
    number_of_people = models.IntegerField()
    number_of_days = models.IntegerField()
    start_date = models.DateTimeField()
    number_of_projects = models.IntegerField()
    view = models.CharField(max_length=100)

    def __str__(self):
        return f"Evaluation for {self.company.name}"

class PracticalIssue(models.Model):
    evaluation = models.ForeignKey(Evaluation, on_delete=models.CASCADE)
    practice_area = models.CharField(max_length=100)
    level = models.CharField(max_length=50)
    practice = models.CharField(max_length=100)
    project_score = models.FloatField()
    project_number = models.IntegerField()
    strength_weakness = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return f"Issue in {self.practice_area} for {self.evaluation.company.name}"
    
class PracticalScore(models.Model):
    evaluation = models.ForeignKey(Evaluation, on_delete=models.CASCADE)
    score = models.FloatField()
    practice_area = models.CharField(max_length=100)
    level = models.CharField(max_length=50, default="1")
    practice = models.CharField(max_length=100)

# 单位申报数据相关表
class CompanyDetail(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=100)
    introduction = models.TextField()
    credit_code = models.CharField(max_length=100)
    department_name = models.CharField(max_length=255)
    max_dev_scale = models.CharField(max_length=50)
    important_grade = models.CharField(max_length=50)
    software_category = models.CharField(max_length=100)
    evaluation_address = models.CharField(max_length=255)
    legal_person_phone = models.CharField(max_length=20)
    contact = models.CharField(max_length=100)
    fax = models.CharField(max_length=20)

class HumanResource(models.Model):
    company_detail = models.ForeignKey(CompanyDetail, on_delete=models.CASCADE)
    type = models.CharField(max_length=100)
    total = models.IntegerField()
    doctor_number = models.IntegerField()
    master_number = models.IntegerField()
    undergraduate_number = models.IntegerField()
    specialty_number = models.IntegerField()

class ResourceConfiguration(models.Model):
    company_detail = models.ForeignKey(CompanyDetail, on_delete=models.CASCADE)
    type = models.CharField(max_length=100)
    name = models.CharField(max_length=255)
    size = models.CharField(max_length=100)
    number = models.IntegerField()

class Project(models.Model):
    company_detail = models.ForeignKey(CompanyDetail, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    develop_area = models.CharField(max_length=100)
    save_key_level = models.CharField(max_length=100)
    cur_stage = models.CharField(max_length=100)
    software_type = models.CharField(max_length=100)
    dev_type = models.CharField(max_length=100)
    start_time = models.DateField()
    end_time = models.DateField()
    dev_partment = models.CharField(max_length=255)
    scale = models.CharField(max_length=100)
    scale_method = models.CharField(max_length=100)
    person_number = models.IntegerField()
    people_day = models.IntegerField()
    fault_number = models.IntegerField()

# class PracticalMean(models.Model):
#     type_1 = models.CharField(max_length=255)
#     type_2 = models.CharField(max_length=255)
#     scope_1 = models.CharField(max_length=255)
#     scope_2 = models.CharField(max_length=255)
#     value = models.FloatField()

# class PeopleMean(models.Model):
#     type_1 = models.CharField(max_length=255)
#     scope_1 = models.CharField(max_length=255)
#     scope_2 = models.CharField(max_length=255)
#     value = models.FloatField()

# class ResouceMean(models.Model):
#     type_1 = models.CharField(max_length=255)
#     scope_1 = models.CharField(max_length=255)
#     scope_2 = models.CharField(max_length=255)
#     value = models.FloatField()

# class PeopleMean(models.Model):
#     type_1 = models.CharField(max_length=255)
#     scope_1 = models.CharField(max_length=255)
#     scope_2 = models.CharField(max_length=255)
#     value = models.FloatField()
