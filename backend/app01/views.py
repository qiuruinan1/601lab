import uuid
from collections import Counter

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from django.db.models import *
import pandas as pd
from app01 import models
import json
from django.core import serializers
import zipfile

import pandas as pd
from app01.models import *
from django.utils.dateparse import parse_datetime
import pytz
from django.utils.dateparse import parse_datetime
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import tarfile
import rarfile
from tempfile import TemporaryDirectory
import os
from app01.dicts import *




# 登陆
def login(request):
    data = {}
    res = json.loads(request.body)
    password = res.get("password")
    name = res.get("name")
    print(name, password)
    use = User.objects.filter(name=name).filter(password=password).values()
    print(len(use))
    data["login"] = len(use)
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


# 注册
def zc(request):
    data = {}
    res = json.loads(request.body)
    password = res.get("password")
    name = res.get("name")
    print(name, password)
    zc = 0
    user = User.objects.filter(name=name)
    if len(user) == 0:
        uid = User(id=uuid.uuid1(), name=name, password=password).save()
        user = User.objects.filter(name=name)
        print(user)
        zc = 1
    data["login"] = zc
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


def home(request):
    data = {}
    res = json.loads(request.body)
    name = res.get("type")
    img = Menu.objects.filter(big_type=name).values("img")[0:5]
    items = Menu.objects.raw(
        'select id, count(type) ,type from menu where big_type = "' + str(name) + '" group by type')
    item = []
    for i in items:
        d = {"label": i.type}
        item.append(d)
    data["img"] = list(img)
    data["items"] = list(item)
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


def info(request):
    res = json.loads(request.body)
    data = {}
    i = res.get("pageIndex")
    size = res.get("pageSize")
    name = res.get("name")
    big = res.get("value")
    num = res.get("num")
    ll = Menu.objects.raw("select id, count(big_type) value,big_type name from menu group by  big_type")
    if num == '':
        li = Menu.objects.filter(name__contains=name).filter(big_type__contains=big).order_by(
            'type').values()[
             (i - 1) * size:i * size]
        total = Menu.objects.filter(name__contains=name).filter(big_type__contains=big).values(
            "id").count()
    else:
        li = Menu.objects.filter(name__contains=name).filter(big_type__contains=big).filter(num=num).order_by(
            'type').values()[
             (i - 1) * size:i * size]
        total = Menu.objects.filter(name__contains=name).filter(big_type__contains=big).filter(num=num).values(
            "id").count()
    item = []
    for i in ll:
        d = {"value": i.name, "label": i.name}
        item.append(d)
    # for i in list(li):
    #     l = i['peiliao'].split(',')
    #     i['num'] = len(l)
    data["items"] = list(item)
    data["list"] = list(li)
    data["total"] = total
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


def echarts(request):
    data = {}
    big = Menu.objects.raw(
        "select id, count(big_type) value,big_type name from menu group by big_type order by value desc limit 20")
    item = []
    for i in big:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["big"] = list(item)
    type = Menu.objects.raw(
        "select id, count(type) value,type name from menu group by type order by value desc limit 50")
    item = []
    for i in type:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["type"] = list(item)
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


def echarts1(request):
    data = {}
    num = Menu.objects.raw(
        "select id, count(num) value,num name from menu group by num order by value desc limit 20")
    item = []
    for i in num:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["num"] = list(item)
    type = Menu.objects.raw(
        "select id,count(num) value,num name from menu where big_type = '中国菜' group by num order by value desc limit 10")
    item = []
    for i in type:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["china"] = list(item)
    wai = Menu.objects.raw(
        "select id,count(num) value,num name from menu where big_type = '外国菜' group by num order by value desc limit 10")
    item = []
    for i in wai:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["wai"] = list(item)
    ge = Menu.objects.raw(
        "select id,count(num) value,num name from menu where big_type = '各地小吃' group by num order by value desc limit 10")
    item = []
    for i in ge:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["ge"] = list(item)
    cs = Menu.objects.raw(
        "select id,count(num) value,num name from menu where big_type = '菜式' group by num order by value desc limit 10")
    item = []
    for i in cs:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["cs"] = list(item)
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


def echarts3(request):
    data = {}
    zg = Menu.objects.raw(
        "select substring_index(sc,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',sc) and big_type = '中国菜' group by name order by value desc limit 10")
    item = []
    for i in zg:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["zg"] = list(item)
    wg = Menu.objects.raw(
        "select substring_index(sc,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',sc) and big_type = '外国菜' group by name order by value desc limit 10")
    item = []
    for i in wg:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["wg"] = list(item)
    gd = Menu.objects.raw(
        "select substring_index(sc,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',sc) and big_type = '各地小吃' group by name order by value desc limit 10")
    item = []
    for i in gd:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["gd"] = list(item)
    cs = Menu.objects.raw(
        "select substring_index(sc,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',sc) and big_type = '菜式' group by name order by value desc limit 10")
    item = []
    for i in cs:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["cs"] = list(item)
    sc = Menu.objects.raw(
        "select   substring_index(sc,'万','1')*10000  value ,count(distinct name), name ,id  "
        " from menu where locate('万',sc)  group by name order by value desc limit 20;")
    item = []
    for i in sc:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["sc"] = list(item)
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})


def echarts4(request):
    data = {}
    zg = Menu.objects.raw(
        "select substring_index(cat,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',cat) and big_type = '中国菜' group by name order by value desc limit 10")
    item = []
    for i in zg:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["zg"] = list(item)
    wg = Menu.objects.raw(
        "select substring_index(cat,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',cat) and big_type = '外国菜' group by name order by value desc limit 10")
    item = []
    for i in wg:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["wg"] = list(item)
    gd = Menu.objects.raw(
        "select substring_index(cat,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',cat) and big_type = '各地小吃' group by name order by value desc limit 10")
    item = []
    for i in gd:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["gd"] = list(item)
    cs = Menu.objects.raw(
        "select substring_index(cat,'万','1')*10000  value ,count(distinct name), name ,id "
        "from menu where locate('万',cat) and big_type = '菜式' group by name order by value desc limit 10")
    item = []
    for i in cs:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["cs"] = list(item)
    cat = Menu.objects.raw(
        "select   substring_index(cat,'万','1')*10000  value ,count(distinct name), name ,id  "
        " from menu where locate('万',cat)  group by name order by value desc limit 20;")
    item = []
    for i in cat:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["cat"] = list(item)
    return JsonResponse(data=data, json_dumps_params={'ensure_ascii':False})


def echarts5(request):
    data = {}
    res = json.loads(request.body)
    data = {}
    p = res.get("name")
    p = '%%' + p + '%%'
    name = Menu.objects.raw(
        "select id, count(zz) value,zz name "
        "from menu group by zz order by value desc limit 100;")
    item = []
    for i in name:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["name"] = list(item)
    print(p)
    sql = "select  cast(substring_index(cat,'万',1) as decimal(10,1))  value ,count(distinct name), name ,id   from menu where locate('万',cat) and peiliao like '" + str(
        p) + "'  group by name order by value desc limit 10"
    print(sql)
    cat = Menu.objects.raw(sql
                                  )
    item = []
    for i in cat:
        d = {"name": i.name, "value": i.value}
        item.append(d)
    data["cat"] = list(item)

    return JsonResponse(data=data, json_dumps_params={'ensure_ascii': False})

def upload_practical_file(request):
    if request.method == 'POST':
        file = request.FILES.get('file')
        print(file.name)
        if file:
            if file.name.endswith('.xlsx') or file.name.endswith('.xls'):
                return handle_excel_file(file)
            # 解压 ZIP 文件并解析 Excel 文件
            elif file.name.endswith('.zip'):
                return handle_zip_file(file)
            elif file.name.endswith('.rar'):
                return handle_rar_file(file)
            elif file.name.endswith('.tar.gz'):
                return handle_tar_gz_file(file)
        else:
            return JsonResponse({'error': 'No file provided'}, status=400)
    return JsonResponse({'error': 'Invalid request'}, status=400)

def handle_rar_file(file):
    try:
        with TemporaryDirectory() as tmpdirname:
            with rarfile.RarFile(file) as rar:
                rar.extractall(tmpdirname)
                for filename in os.listdir(tmpdirname):
                    if filename.endswith(('.xlsx', '.xls')):
                        filepath = os.path.join(tmpdirname, filename)
                        with pd.ExcelFile(filepath) as xls:
                            # 使用 parse_excel 函数处理解压后的 Excel 文件
                            parse_excel(xls)
            return JsonResponse({'message': 'RAR file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)

def handle_tar_gz_file(file):
    try:
        with TemporaryDirectory() as tmpdirname:
            with tarfile.open(fileobj=file, mode='r:gz') as tar:
                tar.extractall(tmpdirname)
                for filename in os.listdir(tmpdirname):
                    if filename.endswith(('.xlsx', '.xls')):
                        filepath = os.path.join(tmpdirname, filename)
                        with pd.ExcelFile(filepath) as xls:
                            # 使用 parse_excel 函数处理解压后的 Excel 文件
                            parse_excel(xls)
            return JsonResponse({'message': 'TAR.GZ file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)
    
def handle_zip_file(zip_file):
    try:
        with zipfile.ZipFile(zip_file, 'r') as zip_ref:
            for filename in zip_ref.namelist():
                if filename.endswith(('.xlsx', 'xls')):
                    excel_file = zip_ref.open(filename)
                    xls = pd.ExcelFile(excel_file)
                    parse_excel(xls)
            return JsonResponse({'message': 'Zip file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)

def handle_excel_file(file):
    try:
        xls = pd.ExcelFile(file)
        parse_excel(xls)
        return JsonResponse({'message': 'Excel file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


def get_type_from_name(name):
    for k, v in industry_types_dict.items():
        if name in v:
            return k

def parse_excel(xls):
    # 读取 Excel 文件
    # xls = pd.ExcelFile(file_path)

    # 解析“基本信息”sheet
    # 获取时区
    timezone = pytz.timezone("Asia/Shanghai")
    df_info = xls.parse('基本信息')
    company_data = {}
    evaluation_data = {}
    for _, row in df_info.iterrows():
        if row['类型'] == '单位名称':
            company_data['name'] = row['内容']
            company_data['industry_type'] = get_type_from_name(row['内容']) 
        elif row['类型'] == '软件从业人数':
            company_data['number_of_employees'] = row['内容']
        # elif row['类型'] == '软件领域':
        #     company_data['industry_type'] = row['内容']
        elif row['类型'] == '评价等级':
            evaluation_data['grade'] = row['内容']
            company_data['grade'] = row['内容']
        elif row['类型'] == '评价形式':
            evaluation_data['form'] = row['内容']
        elif row['类型'] == '评价组人数':
            evaluation_data['number_of_people'] = row['内容']
        elif row['类型'] == '评价所需天数':
            evaluation_data['number_of_days'] = row['内容']
        elif row['类型'] == '评价开始时间':
            # 检查内容是否为字符串
            if isinstance(row['内容'], str):
                naive_datetime = parse_datetime(row['内容'])
            else:
                # 如果内容不是字符串，尝试使用 pandas 的 to_datetime 来解析
                naive_datetime = pd.to_datetime(row['内容'])
            aware_datetime = timezone.localize(naive_datetime)
            evaluation_data['start_date'] = aware_datetime
        elif row['类型'] == '参评项目数':
            evaluation_data['number_of_projects'] = row['内容']
        elif row['类型'] == '评价视图':
            evaluation_data['view'] = row['内容']

    # 将单位信息存储到数据库
    company, created_1 = Company.objects.get_or_create(**company_data)
    # 评价信息需要添加单位外键
    evaluation_data['company'] = company
    # 添加评价信息到数据库
    evaluation, created_2 = Evaluation.objects.get_or_create(**evaluation_data)

    if not created_1 and not created_2: # 防止重复的记录出现
        pass
    else:
        # 解析“实践问题”sheet
        df_issues = xls.parse('实践问题')
        parse_practical_issues_sheet(df_issues, evaluation)

def parse_practical_issues_sheet(df, evaluation):
    # 计算 n 的值（总列数 - 固定的其他列数）
    n = len(df.columns) - 4  # 除去'实践域', '实践', '强弱', '问题描述'

    for _, row in df.iterrows():
        total_score = 0
        level = row['实践'].split(' ')[-1].split('.')[0]
        for i in range(1, n + 1):
            if row[f'P{i}'] == "F":
                score = 100
            else:
                score = 75
            total_score += score
            PracticalIssue.objects.create(
                evaluation=evaluation,
                practice_area=practice_dict[row['实践域']],
                practice=row['实践'],
                project_score=score,
                project_number=i,
                strength_weakness=row['强弱'],
                description=row['问题简述'],
                level = level
            )
        PracticalScore.objects.create(
            evaluation=evaluation,
            score=round(total_score/n, 3),
            practice_area=practice_dict[row['实践域']],
            practice=row['实践'],
            level = level
        )

@require_http_methods(["POST"])
def upload_json_file(request):
    # try:
    #     file = request.FILES.get('file')
    #     if file:
    #         # 读取并解析 JSON 文件
    #         try:
    #             if file.name.endswith('.xlsx') or file.name.endswith('.xls'):
    #                 return handle_excel_file(file)
    #             # 解压 ZIP 文件并解析 Excel 文件
    #             elif file.name.endswith('.zip'):
    #                 return handle_zip_file_json(file)
    #             elif file.name.endswith('.rar'):
    #                 return handle_rar_file_json(file)
    #             elif file.name.endswith('.tar.gz'):
    #                 return handle_tar_gz_file_json(file)
    #             elif file.name.endswith('.json'):
    #                 data = json.load(file)
    #                 parse_json(data)
    #             return JsonResponse({'message': 'JSON file processed successfully'})
    #         except json.JSONDecodeError as e:
    #             return JsonResponse({'error': 'Invalid JSON file'}, status=400)
    #         except Exception as e:
    #             return JsonResponse({'error': str(e)}, status=500)
    #     else:
    #         return JsonResponse({'error': 'No file provided'}, status=400)
    # except Exception as e:
    #     return JsonResponse({"error": str(e)}, status=500)
    file = request.FILES.get('file')
    if file:
        # 读取并解析 JSON 文件
            if file.name.endswith('.xlsx') or file.name.endswith('.xls'):
                return handle_excel_file(file)
            # 解压 ZIP 文件并解析 Excel 文件
            elif file.name.endswith('.zip'):
                return handle_zip_file_json(file)
            elif file.name.endswith('.rar'):
                return handle_rar_file_json(file)
            elif file.name.endswith('.tar.gz'):
                return handle_tar_gz_file_json(file)
            elif file.name.endswith('.json'):
                data = json.load(file)
                parse_json(data)
            return JsonResponse({'message': 'JSON file processed successfully'})
    else:
        return JsonResponse({'error': 'No file provided'}, status=400)


def handle_rar_file_json(file):
    try:
        with TemporaryDirectory() as tmpdirname:
            with rarfile.RarFile(file) as rar:
                rar.extractall(tmpdirname)
                for filename in os.listdir(tmpdirname):
                    if filename.endswith('.json'):
                        filepath = os.path.join(tmpdirname, filename)
                        with open(filepath, 'r', encoding ='utf-8') as f:
                            data = json.load(f)
                            parse_json(data)
            return JsonResponse({'message': 'RAR file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)

def handle_tar_gz_file_json(file):
    try:
        with TemporaryDirectory() as tmpdirname:
            with tarfile.open(fileobj=file, mode='r:gz') as tar:
                tar.extractall(tmpdirname)
                for filename in os.listdir(tmpdirname):
                    if filename.endswith('.json'):
                        filepath = os.path.join(tmpdirname, filename)
                        with open(filepath, 'r', encoding ='utf-8') as f:
                            data = json.load(f)
                            parse_json(data)
            return JsonResponse({'message': 'TAR.GZ file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)
    
def handle_zip_file_json(zip_file):
    try:
        with zipfile.ZipFile(zip_file, 'r') as zip_ref:
            for filename in zip_ref.namelist():
                if filename.endswith('.json'):
                    json_file = zip_ref.open(filename)
                    data = json.load(json_file)
                    parse_json(data)
            return JsonResponse({'message': 'Zip file processed successfully'})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)

def parse_json(data):
    company_detail = {}
    human_resource = {}
    resource_config = {}
    project_info = {}
    # 单位详情
    company_detail['name'] = data[0]['data']['unitName']
    company_detail['code'] = data[0]['data']['unitCode']
    company_detail['introduction'] = data[0]['data']['unitIntroduction']
    company_detail['credit_code'] = data[0]['data']['creditCode']
    company_detail['department_name'] = data[0]['data']['departmentName']
    company_detail['max_dev_scale'] = data[0]['data']['maxDevScale']
    company_detail['important_grade'] = data[0]['data']['impotentGrade']
    company_detail['software_category'] = data[0]['data']['softwareCategory']
    company_detail['evaluation_address'] = data[0]['data']['evaluationAddress']
    company_detail['legal_person_phone'] = data[0]['data']['legalPersonPhone']
    company_detail['contact'] = data[0]['data']['contact']
    company_detail['fax'] = data[0]['data']['fax']
    company = Company.objects.get(name=company_detail['name'])
    company_detail['company'] = company
    company_detail, created_1 = CompanyDetail.objects.get_or_create(**company_detail)
    if not created_1:
        return
    # 人力资源
    for item in data[2]['data']:
        human_resource['type'] = item['personType']
        human_resource['total'] = item['total']
        human_resource['doctor_number'] = item['doctor']
        human_resource['master_number'] = item['master']
        human_resource['undergraduate_number'] = item['undergraduate']
        human_resource['specialty_number'] = item['specialty']
        human_resource['company_detail'] = company_detail
        HumanResource.objects.create(**human_resource)
    # 软件资源
    for item in data[3]['data']:
        resource_config['type'] = item['softwareType']
        resource_config['name'] = item['softwareName']
        resource_config['size'] = item['size']
        resource_config['number'] = item['number']
        resource_config['company_detail'] = company_detail
        ResourceConfiguration.objects.create(**resource_config)
    # 项目信息
    project_number = (len(data) - 15) // 2 + 1
    for i in range(project_number):
        item = data[7+i*2]['data']
        if len(item) == 0:
            continue
        # print(item)
        project_info['name'] = item['projectName']
        project_info['develop_area'] = item['developArea']
        project_info['save_key_level'] = item['saveKeyLevel']
        project_info['cur_stage'] = item['curStage']
        project_info['software_type'] = item['softwareType']
        project_info['dev_type'] = item['devType']
        project_info['start_time'] = item['startTime']
        project_info['end_time'] = item['endTime']
        project_info['dev_partment'] = item['devPartment']
        project_info['scale'] = item['preSoftwareScale']
        project_info['scale_method'] = item['scaleMethod']
        project_info['person_number'] = item['xmzrs']
        project_info['people_day'] = item['sjgzl']
        project_info['fault_number'] = item['yscsqxs']
        project_info['company_detail'] = company_detail
        Project.objects.create(**project_info)

@require_http_methods(["POST"])
def practice_domain_analysis(request):
    try:
        # 解析请求数据
        data = json.loads(request.body)
        industry_type = data.get('industry_type')
        evaluate_level = data.get('evaluate_level')
        organize_name = data.get('organze_name')

        # 根据请求数据查询相关记录（包含全量查询）
        if industry_type == '全部' and evaluate_level == '全部':
            companies = Company.objects.filter()
            evaluations = Evaluation.objects.filter(company__in=companies)
        elif industry_type == '全部':
            companies = Company.objects.filter()
            evaluations = Evaluation.objects.filter(company__in=companies, grade=evaluate_level)
        elif evaluate_level == '全部':
            companies = Company.objects.filter(industry_type=industry_type)
            evaluations = Evaluation.objects.filter(company__in=companies)
        elif organize_name == '':
            companies = Company.objects.filter(industry_type=industry_type)
            evaluations = Evaluation.objects.filter(company__in=companies, grade=evaluate_level)
        else:
            companies = Company.objects.filter(industry_type=industry_type, name=organize_name)
            evaluations = Evaluation.objects.filter(company__in=companies, grade=evaluate_level)
        practice_scores = PracticalScore.objects.filter(evaluation__in=evaluations)

        # 统计每个实践域的得分情况
        domain_analysis = {}
        for score in practice_scores:
            domain = score.practice_area
            if domain not in domain_analysis:
                domain_analysis[domain] = [0, 0, 0, 0]  # 通过，待改进，弱项，总数

            domain_analysis[domain][3] += 1
            if score.score >= 100:
                domain_analysis[domain][0] += 1
            elif score.score > 75:
                domain_analysis[domain][1] += 1
            else:
                domain_analysis[domain][2] += 1

        # 格式化输出
        formatted_data = [{"实践域": k, "统计": [int(i / len(evaluations)) for i in v]} for k, v in domain_analysis.items()]

        return JsonResponse({"data": formatted_data}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=500)
    
def get_industry_types(request):
    if request.method == 'GET':
        # 获取所有不同的行业类别
        industry_types = Company.objects.values_list('industry_type', flat=True).distinct()
        return JsonResponse({'industry_types': list(industry_types)})
    else:
        return JsonResponse({'error': 'Invalid request method'}, status=405)
    
def get_companies_by_criteria(request):
    try:
        # 提取请求参数
        industry_type = request.GET.get('industry_type')
        evaluate_level = request.GET.get('evaluate_level')

        # if industry_type == "全部" and evaluate_level == "全部":
        #     companies = Company.objects.filter()
        # elif industry_type == "全部":
        #     companies = Company.objects.filter(evaluation__grade=evaluate_level)
        # elif evaluate_level == "全部":
        #     companies = Company.objects.filter(industry_type=industry_type)
        # else:
        #     companies = Company.objects.filter(industry_type=industry_type, evaluation__grade=evaluate_level)
        companies = Company.objects.filter(industry_type=industry_type, evaluation__grade=evaluate_level)

        # 查询数据库
        companies = companies.distinct().values_list('name', flat=True)
        return_list = []
        for company in companies:
            return_list.append({"value": company})
        # 返回单位名称列表
        return JsonResponse({'companies': return_list})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)
    

# 定义一个函数来处理提供的 JSON 数据
def aggregate_json_data(json_data, aggregate_name):
    result = {}
    formatted_data = []
    for item in json_data['data']:
        # 获取当前条目的类型和数量
        person_type = item[aggregate_name]
        count = item['数量']

        # 如果类型已经在结果中，累加数量；否则，添加新的条目
        if person_type in result:
            result[person_type] += count
        else:
            result[person_type] = count
    for k, v in result.items():
        formatted_data.append({aggregate_name: k, '数量': v})
    return formatted_data

def resource_config_analysis(request):
    try:
        # 解析请求数据
        data = json.loads(request.body)
        industry_type = data.get('industry_type')
        evaluate_level = data.get('evaluate_level')
        organize_name = data.get('organze_name')

        # 根据请求数据查询相关记录（包含全量查询）
        if industry_type == '全部' and evaluate_level == '全部':
            companies = Company.objects.filter()
        elif industry_type == '全部':
            companies = Company.objects.filter(grade=evaluate_level)
        elif evaluate_level == '全部':
            companies = Company.objects.filter(industry_type=industry_type)
        elif organize_name == '':
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level)
        else:
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level, name=organize_name)
        company_details = CompanyDetail.objects.filter(company__in=companies)
        resources = ResourceConfiguration.objects.filter(company_detail__in=company_details)
        formatted_data = []
        for resource in resources:
            formatted_data.append({"资产类型": resource.type, "数量": resource.number})
        result = aggregate_json_data({"data": formatted_data}, "资产类型")
        for item in result:
            item["数量"] = int(item["数量"] / len(company_details))
        return JsonResponse({"data": result}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=500)
        解析请求数据
    
def human_resource_analysis(request):
    try:
        # 解析请求数据
        data = json.loads(request.body)
        industry_type = data.get('industry_type')
        evaluate_level = data.get('evaluate_level')
        organize_name = data.get('organze_name')

        # 根据请求数据查询相关记录（包含全量查询）
        if industry_type == '全部' and evaluate_level == '全部':
            companies = Company.objects.filter()
        elif industry_type == '全部':
            companies = Company.objects.filter(grade=evaluate_level)
        elif evaluate_level == '全部':
            companies = Company.objects.filter(industry_type=industry_type)
        elif organize_name == '':
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level)
        else:
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level, name=organize_name)
        company_details = CompanyDetail.objects.filter(company__in=companies)
        humans = HumanResource.objects.filter(company_detail__in=company_details)
        formatted_data = []
        for human in humans:
            formatted_data.append({"人员类型": human.type, "数量": human.total})

        result = aggregate_json_data({"data": formatted_data}, "人员类型")
        for item in result:
            item["数量"] = int(item["数量"] / len(company_details))
        return JsonResponse({"data": result}, safe=False)

    except Exception as e:
        return JsonResponse({"error": str(e)}, status=500)

def efficiency_analysis(request):
    try:
        # 解析请求数据
        data = json.loads(request.body)
        industry_type = data.get('industry_type')
        evaluate_level = data.get('evaluate_level')
        organize_name = data.get('organze_name')
        is_mean = data.get("is_mean")

        # 根据请求数据查询相关记录（包含全量查询）
        if industry_type == '全部' and evaluate_level == '全部':
            companies = Company.objects.filter()
        elif industry_type == '全部':
            companies = Company.objects.filter(grade=evaluate_level)
        elif evaluate_level == '全部':
            companies = Company.objects.filter(industry_type=industry_type)
        elif organize_name == '':
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level)
        else:
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level, name=organize_name)
        company_details = CompanyDetail.objects.filter(company__in=companies)
        formatted_data = []
        projects_num = 0
        scale_total = 0
        people_day_total = 0
        for company_detail in  company_details:
            projects = Project.objects.filter(company_detail=company_detail)
            projects_num += len(projects)
            this_company_projects_info = []
            for project in projects:
                this_company_projects_info.append({"项目名称": project.name, "代码规模": int(project.scale), '人天信息': project.people_day, '效率': round(int(project.scale) / project.people_day, 2)})
                scale_total += int(project.scale)
                people_day_total += project.people_day
            formatted_data.append({"单位名称": company_detail.name, "项目列表": this_company_projects_info})
        if is_mean:
            return JsonResponse({'data': {"代码规模": int(scale_total / projects_num), "人天信息": int(people_day_total / projects_num), "效率": round(scale_total / people_day_total, 2)}})
        else:
            return JsonResponse({"data": formatted_data}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=500)
    
def get_company_details(request):
    try:
        # 解析请求数据
        data = json.loads(request.body)
        industry_type = data.get('industry_type')
        evaluate_level = data.get('evaluate_level')
        organize_name = data.get('organze_name')

        # 根据请求数据查询相关记录（包含全量查询）
        if industry_type == '全部' and evaluate_level == '全部':
            companies = Company.objects.filter()
        elif industry_type == '全部':
            companies = Company.objects.filter(grade=evaluate_level)
        elif evaluate_level == '全部':
            companies = Company.objects.filter(industry_type=industry_type)
        elif organize_name == '':
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level)
        else:
            companies = Company.objects.filter(industry_type=industry_type, grade=evaluate_level, name=organize_name)
        company_details = CompanyDetail.objects.filter(company__in=companies)
        formatted_data = []
        for company_detail in company_details:
            formatted_data.append({
                "单位名称": company_detail.name, 
                "单位代号": company_detail.code,
                '单位简介': company_detail.introduction,
                "统一社会信用代码": company_detail.credit_code,
                "所属部门": company_detail.department_name,
                "最大开发规模": company_detail.max_dev_scale,
                "最高重要性等级": company_detail.important_grade,
                "承研软件类型": company_detail.software_category,
                "现场评价地址": company_detail.evaluation_address,
                "联系电话": company_detail.legal_person_phone,
                "联系人": company_detail.contact,
                "传真": company_detail.fax
                })
        return JsonResponse({"data": formatted_data}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=500)

    