"""djangoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from app01 import views

urlpatterns = [
    path('api/admin/', admin.site.urls),
    path('api/login/', views.login),
    path('api/zc/', views.zc),
    path('api/index/', views.home),
    path('api/info/', views.info),
    path('api/echarts/', views.echarts),
    path('api/echarts1/', views.echarts1),
    path('api/echarts3/', views.echarts3),
    path('api/echarts4/', views.echarts4),
    path('api/echarts5/', views.echarts5),
    path('api/upload_1', views.upload_practical_file),
    path('api/upload_2', views.upload_json_file),
    path('api/practice_domain_analysis', views.practice_domain_analysis),
    path('api/get_industry_types', views.get_industry_types),
    path('api/get_company_names', views.get_companies_by_criteria),
    path('api/resource-allocation-analysis', views.resource_config_analysis),
    path('api/staff-allocation-analysis', views.human_resource_analysis),
    path('api/efficiency-analysis', views.efficiency_analysis),
    path('api/get-company-details', views.get_company_details),

]
