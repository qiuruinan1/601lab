# hotelvue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

#### 结构
``` 
├─public
└─src
    ├─assets 静态资源
    ├─components 公共组件
    ├─router 路由
    ├─store 
    └─views 页面
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
