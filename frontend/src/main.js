import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import zh from 'element-ui/lib/locale/lang/zh-CN'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import axios from "axios";
import dataV from '@jiaminghi/data-view'
import { borderBox6 } from '@jiaminghi/data-view'
Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.use(dataV)
Vue.use(borderBox6)
Vue.use(ElementUI, {
  zh
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
