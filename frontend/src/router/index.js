import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '@/views/login'),
    },
    {
        path: '/in',
        name: 'home',
        component: HomeView,
        redirect: '/index',
        children: [

            {
                path: '/index',
                name: 'index',
                component: () => import(/* webpackChunkName: "about" */ '@/views/index/index')
            },
            {
                path: '/list',
                name: 'list',
                component: () => import(/* webpackChunkName: "about" */ '@/views/menu/list')
            },
            {
                path: '/data-import-module',
                name: 'data-import-module',
                component: () => import(/* webpackChunkName: "about" */ '@/views/menu/dataimport')
            },
            {
                path: '/parctice_domain_analyze',
                name: 'parctice_domain_analyze',
                component: () => import(/* webpackChunkName: "about" */ '@/views/echarts/echarts-1')
            },
            {
                path: '/resource-allocation-analysis',
                name: 'resource-allocation-analysis',
                component: () => import(/* webpackChunkName: "about" */ '@/views/echarts/echarts-3')
            },
            {
                path: '/staff-allocation-analysis',
                name: 'staff-allocation-analysis',
                component: () => import(/* webpackChunkName: "about" */ '@/views/echarts/echarts-2')
            },
            {
                path: '/software-dev-efficiency-analysis',
                name: 'software-dev-efficiency-analysis',
                component: () => import(/* webpackChunkName: "about" */ '@/views/echarts/echarts-4')
            },
            {
                path: '/echarts4',
                name: 'echarts4',
                component: () => import(/* webpackChunkName: "about" */ '@/views/echarts/echarts-4')
            },
            {
                path: '/echarts5',
                name: 'echarts5',
                component: () => import(/* webpackChunkName: "about" */ '@/views/echarts/echarts-5')
            }
        ]
    },

    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
