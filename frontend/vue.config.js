const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    //解决跨域问题
    proxy: {
      '/api': {
        target: 'http://192.168.31.119:8000',
        changeOrigin: true,
      }
    },
  },
})
